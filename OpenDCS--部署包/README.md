#OpenDCS(仅限Windows使用)部署包
----
##部署方法
###1.安装OpenOffice和Java必备环境
Openoffice: http://www.openoffice.org/ 

Java: https://www.java.com/
###2.启动Openoffice服务
先cd到openoffice下的program目录，然后执行命令：
```
soffice -headless -accept="socket,host=127.0.0.1,port=8100;urp;" -nofirststartwizard 
```
###3.部署OpenDCS工程
解压 **OpenDCS1.0.zip** 到任一目录，配置 **config.ini** 
Host--主机名，默认为空( **全部未分配** )
Port--端口，默认8081
```
[Server]
Host=
Port=8081
Console=true
```
###4.运行OpenDCS
打开OpenDCS.exe，访问
```
http://你的IP或域名:你的端口
```
会看到如下图所示
![主页](https://git.oschina.net/uploads/images/2017/0725/120410_95c019f8_906045.png "OpenDCS1.png")
输入文档网络地址，获得预览地址
![预览](https://git.oschina.net/uploads/images/2017/0725/120637_d000fb78_906045.png "OpenDCS2.png")
访问
![加载中](https://git.oschina.net/uploads/images/2017/0725/120806_ebd5e0e7_906045.png "OpenDCS3.png")
###预览API
地址(请确保在同一局域网内)

“要预览的文档地址”是完整的http地址
```
http://你的IP或域名:你的端口/web/view?src=要预览的文档地址
```
###清除缓存
删掉local文件夹即可
###常见问题
未启动服务，见步骤2
```
ERROR: connection failed. Please make sure OpenOffice.org is running and listening on port 8100.
```

By 王逸伦